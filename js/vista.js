var vista = {

    getElemPeli: function getElemPeli(infoPeli) {
        var div = document.createElement('div');
        div.setAttribute('class', '');
        div.innerHTML = `
			<a id="myBtn">
             <img src="${infoPeli.imagen}" class="img-responsive" />
			 <!-- <h3>${infoPeli.nombre}</h3>
			 <p>Año: ${infoPeli.año}</p>
			 <p>Genero: ${infoPeli.genero}</p> -->
            </a>

            `;

        var that = this;

        div.querySelector('#myBtn').addEventListener('click', function () {
            var modal = that.getModalPeli(infoPeli);
            div.appendChild(modal);
        });

        return div;
    },

    getModalPeli: function getModalPeli(infoPeli) {
        var div = document.createElement('div');

        div.innerHTML = `
             <div class="modal-backdrop fade"></div>
              <div class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h2 class="modal-title">${infoPeli.nombre}</h2>
                        <p style="font-size: 32px;"><a style="color: rgb(255, 238, 0);">&#9733;</a>${infoPeli.popularidad} </p>
                    </div>
                    <div class="modal-body">
                      <img src="${infoPeli.imagen}" class="img-responsive" width="200" height="300"/>
                        <div class="modal-caracteristicasPeli">
                            <p><b>Género:</b> ${infoPeli.genero}</p>
                            <p><b>Año:</b> ${infoPeli.año}</p>
                            <p><b>Idioma:</b> ${infoPeli.idioma}</p>
                            <p>${infoPeli.sinopsis}</p>
                        </div>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
              </div><!-- /.modal -->
            `;

        var modal = div.querySelector('.modal');
        var backdrop = div.querySelector('.modal-backdrop');

        modal.style.display = 'block';

        setTimeout(function () {
            modal.style.opacity = 1;
            backdrop.style.opacity = .5;
            div.querySelector('.modal-dialog').style.transform = 'translate(0,0)';
        });

        var remove = function () {

            modal.style.opacity = 0;
            backdrop.style.opacity = 0;
            setTimeout(function () {
                div.remove();
            }, 300);
        };
        modal.addEventListener('click', function (e) {
            if (e.target == modal) remove();
        });
        div.querySelector('button').addEventListener('click', remove);

        return div;
    },

    getElemPelis: function getElemPelis(listaPelis) {
        var div = document.createElement('div');
        div.setAttribute('class', 'contenedorPelis');

        var that = this;
        listaPelis.forEach(function (infoPeli) {
            var li = that.getElemPeli(infoPeli);
            div.appendChild(li);
        });


        return div;
    },

    getHeader: function getHeader() {
        var header = document.querySelector('header');

        var filterTipo = header.querySelector('#tipo');
        var filterGenero = header.querySelector('#genero');
        var filterAño = header.querySelector('#año');
        var filterPopul = header.querySelector('#popularidad');
        var filterIdioma = header.querySelector('#idioma');

        var that = this;
        var miFunc = function () {
            console.log(filterTipo.tip.value, filterGenero.value, filterAño.value, filterPopul.puntaje.value, filterIdioma.value);

            that.onFilter(filterTipo.tip.value, filterGenero.value, filterAño.value, filterPopul.puntaje.value, filterIdioma.value);
        };

        filterTipo.addEventListener('change', miFunc);
        filterGenero.addEventListener('change', miFunc);
        filterAño.addEventListener('change', miFunc);
        filterPopul.addEventListener('change', miFunc);
        filterIdioma.addEventListener('change', miFunc);
    },

    render: function render(listaPelis) {
        var main = document.getElementById('main');

        main.setAttribute('class', '');

        var elemPelis = this.getElemPelis(listaPelis);

        main.innerHTML = '';
        main.appendChild(elemPelis);
    },

};

function myFunction() {
    //   document.getElementById("load").classList.add("desaparecer");
    document.getElementById("load").classList.add("transition");

    showSlides();

    setTimeout(function () {
        alert("Este trabajo merece un 5");
        document.getElementById("load").classList.add("desparecer");
    }, 3000);

};

var slideIndex = 0;

function showSlides() {
    var i;
    var slides = document.getElementsByClassName("mySlides");
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    slideIndex++;
    if (slideIndex > slides.length) {
        slideIndex = 1
    }
    slides[slideIndex - 1].style.display = "block";
    setTimeout(showSlides, 2000); // Change image every 2 seconds
};

vista.getHeader();
