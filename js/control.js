var control = function control(vista, peliculas) {

    vista.onFilter = function onFilter(tipo, genero, año, popularidad, idioma) {

        var pelisFiltradas = peliculas
            .filter(function (elem) {
                if (tipo == '') return true;

                return elem.tipo == tipo;
            })
            .filter(function (elem) {
                if (genero == '') return true;
                return elem.genero == genero;
            })
            .filter(function (elem) {
                if (!año) return true;

                var rango = año.split('-');

                rango[0] = parseInt(rango[0]);
                rango[1] = parseInt(rango[1]);

                return elem.año >= rango[0] && elem.año < rango[1];
            }).
        filter(function (elem) {
            if (popularidad == 0) return true;

            popularidad = parseInt(popularidad);
            return elem.popularidad == popularidad;
        }).
        filter(function (elem) {
            if (idioma == '') return true;
            return elem.idioma == idioma;
        });

        vista.render(pelisFiltradas);
    }

    vista.render(peliculas);
}

control(vista, peliculas);
