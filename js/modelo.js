var peliculas = [
    {
        tipo: "Pelicula",
        genero: "Animacion",
        nombre: "Moana",
        año: 2016,
        popularidad: 4,
        idioma: "Español", 
        sinopsis: "Hace miles de años los mejores marineros del mundo viajaban a lo largo del océano Pacífico descubriendo nuevas islas, pero un buen día sus viajes cesaron y aún nadie ha descubierto porqué. Moana, una joven apasionada e intrépida que siente un fuerte vínculo con el mar, es la hija adolescente del líder de una tribu que habita las islas del Pacífico Sur. Ella está dispuesta a resolver el misterio sobre sus antepasados. Siguiendo el consejo de su abuela, la joven decide hacer oídos sordos a la prohibición de su padre de permanecer en la isla, y se lanza a los confines del Océano en compañía de su mascota, un torpe gallo de nombre Heihei. En su camino se encontrarán con Maui, un semidiós que cuenta con un anzuelo mágico, que le da la habilidad de cambiar de forma. Juntos vivirán una gran aventura en la que se enfrentarán a feroces criaturas en una misión casi imposible que les llevará a cruzar el mar abierto, encontrando a su paso mucha acción, enormes animales marinos, submundos sorprendentes y culturas antiguas. ",
        imagen: "./img/Moana.jpg"
    },
    {
        tipo: "Pelicula",
        genero: "Accion",
        nombre: "John Wick",
        año: 2014,
        popularidad: 4,
        idioma: "Ingles", 
        sinopsis: "John Wick, interpretado por Keanu Reeves es un antiguo asesino a sueldo que observa impasible cómo un ladrón roba su coche, un Ford Mustang del 69, y mata al perro que su mujer le regaló. Wick entonces persigue al delincuente y descubre que su padre es un jefe mafioso de Nueva York y que ha puesto una alta recompensa a cambio de su cabeza. Casualmente la persona contratada para darle caza es un sicario, encarnado por Willem Dafoe (Spiderman), un gran amigo y ex compañero suyo, que le recomendó abandonar la peligrosa profesión después de la muerte de su mujer.",
        imagen: "./img/JohnWick.jpg"
    },
    {
        tipo: "Pelicula",
        genero: "Animacion",
        nombre: "El libro de la vida",
        año: 2014,
        popularidad: 5,
        idioma: "Español",
        sinopsis: "Del productor Guillermo del Toro, llega El libro de la vida, película de animación dirigida por Jorge R. Gutiérrez. Con un estilo visual único, contará las aventuras de Manolo, un torero enamorado de una bella mujer llamada María, que ha de decidir entre su familia y su mayor pasión: la música. Antes de elegir qué camino tomar, Manolo se embarcará en un increíble viaje que abarcará tres mundos fantásticos, en los que deberá encararse a sus peores pesadillas y miedos. Como telón de fondo, la película nos situará en México en el trascurso de la conocida tradición del Día de los Muertos.",
        imagen: "./img/Libro.jpg"
    },
    {
        tipo: "Pelicula",
        genero: "Romance",
        nombre: "El diario de Noa",
        año: 2004,
        popularidad: 3,
        idioma: "Español",
        sinopsis: "A Allie Hamilton (Rachel McAdams, Todos los días de mi vida) una chica que proviene de familia adinerada, le importa muy poco que el chico que conoce en Seabrook, durante las vacaciones de verano, se gane la vida de forma humilde como empleado de una fábrica de madera. El amor que siente por él desde el primer momento de conocerse va más allá de los prejuicios económicos y sociales que tantas veces quitan el sueño a los de su clase. Lo mejor de todo es que Noa (Ryan Gosling, Los idus de marzo), que así se llama el chico elegido, siente la misma atracción y amor por ella. No obstante, y a pesar de la pureza de sus sentimientos, la relación entre Noah y Allie parece estar destinada al fracaso. Primero serán los padres de la chica los que se opongan firmemente a este noviazgo y tomen la resolución de llevarse a Allie del pueblo; más tarde será un conflicto bélico el que mantenga a la pareja separada y por último, ambos tendrán que enfrentarse a la terrible enfermedad del olvido para que la historia no desaparezca de su recuerdo.",
        imagen: "./img/Noa.jpg"
    },
    {
        tipo: "Pelicula",
        genero: "Drama",
        nombre: "Precious",
        año: 2010,
        popularidad: 2,
        idioma: "Ingles",
        sinopsis: "Claireece \"Preciosa\" Jones (Gabourey Sidibe) es una adolescente que sufrió muchos problemas durante su juventud. Sus padres no la trataron bien y terminó creciendo siendo infeliz por carecer de cualquier tipo de muestra de cariño. Preciosa no ha tenido una educación escolar básica y ahora su meta principal es comenzar a estudiar. No lo tendrá fácil, sus carencias, el sobrepeso y el embarazo de su segundo hijo serán pequeños baches que tendrá que superar para huir de un pasado traumático y buscar un refugio en su imaginación.",
        imagen: "./img/Precious.jpg"
    },
    {
        tipo: "Pelicula",
        genero: "Sci-Fi",
        nombre: "Avatar",
        año: 2009,
        popularidad: 3,
        idioma: "Ingles",
        sinopsis: "Corre el año 2154. Jake Sully es un antiguo marine que vive postrado en una silla de ruedas, pero cuya determinación y valentía se mantienen intactas. Ahora, tiene una misión entre manos: viajar a Pandora, un planeta del que los humanos extraen un mineral que puede acabar con la crisis energética de la Tierra. Sin embargo, este planeta posee una atmósfera extremadamente tóxica, por lo que se ha creado el programa Avatar, según el cual los humanos unen sus conciencias a un cuerpo biológico que, por control remoto, es capaz de sobrevivir al aire letal. Este cuerpo está compuesto tanto por ADN humano como por ADN de los Na'vis, las criaturas nativas de Pandora, entre las que Jake se tendrá que infiltrar empleando su nueva apariencia. Gracias a su avatar, Jake es capaz de caminar otra vez y eso, junto con el hecho de conocer a Neytiri, una bella Na'vi, provocará que disfrute cada vez más de su apariencia alienígena y menos de la humana. Poco a poco, será aceptado en la tribu y estrechará lazos con las criaturas, mientras los humanos esperan informes de la misión y se preparan para entrar en batalla con los Na'vis, lo único que se interpone entre ellos y el codiciado mineral. ",
        imagen: "./img/Avatar.jpg"
    },
    {
        tipo: "Pelicula",
        genero: "Terror",
        nombre: "Anabelle",
        año: 2014,
        popularidad: 3,
        idioma: "Ingles",
        sinopsis: "Spin-off de la diabólica muñeca Annabelle, que aparecía en la película Expediente Warren: The Conjuring. Siguiendo el hilo argumental del anterior filme, una pareja comienza a sufrir experiencias terroríficamente supernaturales en su hogar debido a una muñeca vintage. A raíz de estos extraños sucesos, el hogar se ve invadido de elementos satánicos para controlar a la muñeca poseída por un diablo.",
        imagen: "./img/Annabelle.jpg"
    },
    {
        tipo: "Pelicula",
        genero: "Musical",
        nombre: "La la land",
        año: 2017,
        popularidad: 4,
        idioma: "Español",
        sinopsis: "Mia (Emma Stone) es una de las muchas aspirantes a actriz que viven en Los Angeles en busca del sueño hollywoodiense, se gana la vida como camarera mientras se presenta a montones de pruebas de casting. Sebastian (Ryan Gosling) es un pianista que vive de las actuaciones de segunda que le salen, y su sueño es regentar su propio club donde rendir tributo al jazz más puro. Los destinos de Mia y Sebastian se cruzarán y la pareja descubrirá el amor, un vínculo que hará florecer y luego poner en jaque las aspiraciones de ambos. En una competición constante por buscar un hueco en el mundo del espectáculo, la pareja descubrirá que el equilibrio entre el amor y el arte puede ser el mayor obstáculo de todos. ",
        imagen: "./img/Lalaland.jpg"
    },
    {
        tipo: "Pelicula",
        genero: "Musical",
        nombre: "Grease(Brillantina)",
        año: 1978,
        popularidad: 3,
        idioma: "Ingles",
        sinopsis: "Al término de las vacaciones de verano, los novios Danny Zuko y Sandy Olsson, una joven australiana de buena familia, se ven obligados a separarse. A su regreso al instituto Rydell, el joven se reúne con su banda, los T-birds, de chaquetas de cuero y pelo engominado. Los padres de Sandy deciden mudarse a Estados Unidos y la chica ingresa en el mismo colegio. Pasada la sorpresa del reencuentro y con el objetivo de quedar bien delante de sus compañeros, Danny adopta una actitud arrogante que deja a la joven totalmente desamparada. Sandy se une entonces a las Pink Ladies, la contraparte femenina de los T-Birds. Entonces tiene lugar un juego del gato y el ratón entre los dos tortolitos, todo ello marcado por los acontecimientos de sus vidas estudiantiles: el comienzo de la temporada de fútbol, el baile de promoción, carreras de coches, noches de chicas, de chicos, salidas al \“fast-food\”, al autocine.",
        imagen: "./img/Grease.jpg"
    },
    {
        tipo: "Pelicula",
        genero: "Musical",
        nombre: "High school musical: 3",
        año: 2008,
        popularidad: 2,
        idioma: "Español",
        sinopsis: "En esta tercera entrega del famoso musical de Disney, Troy y Gabriella ya son adultos. Afrontan el último año de la escuela secundaria y, pronto, en la Universidad, sus caminos tomarán vías diferentes. Para reflejar sus experiencias en el instituto y sus visiones de cara al futuro, Troy y Gabriella participarán junto al resto de los Wildcats en un nuevo número musical de primavera más espectacular que nunca.",
        imagen: "./img/HighSchool.jpg"
    },
    {
        tipo: "Pelicula",
        genero: "Comedia",
        nombre: "Juntos y revueltos",
        año: 2014,
        popularidad: 2,
        idioma: "Español",
        sinopsis: "Un hombre y una mujer van un resort con sus respectivas familias y no pueden ocultar su sorpresa al verse el uno al otro. Ya se conocían, había tenido una cita a ciegas hace algún tiempo, un encuentro cuyo resultado no había sido para nada satisfactorio. No obstante, contra todo pronóstico, la atracción que entonces no sintieron en su momento hace ahora su aparición. Pero ellos no son los únicos que se ven beneficiados por esto, sus hijos también saben sacar partido a la situación, convirtiendo la creciente relación en algo de lo que pueden sacar provecho.",
        imagen: "./img/JuntosYRevueltos.jpg"
    },
    {
        tipo: "Pelicula",
        genero: "Accion",
        nombre: "007 Spectre",
        año: 2017,
        popularidad: 4,
        idioma: "Ingles",
        sinopsis: "Un críptico mensaje del pasado envía a James Bond (Daniel Craig) a una misión no autorizada, para destapar una misteriosa y siniestra organización. Esta nueva amenaza para el Gobierno británico podría acabar con la paz y la seguridad de todo el planeta. Por eso, para obtener la información necesaria y desactivar los planes terroristas, los servicios secretos encargarán al conflictivo y desobediente Agente Bond esta misión. 007 viajará a Ciudad de México y finalmente a Roma, donde conocerá a Lucia Sciarra (Monica Bellucci), la bella y peligrosa viuda de un criminal infame. Cuando Bond se infiltra en una reunión secreta, descubrirá una siniestra organización conocida como SPECTRE. Será a medida que se adentre en el corazón de SPECTRE, como averiguará la conexión escalofriante que existe con el enemigo que busca: Franz Oberhauser (Christoph Waltz).",
        imagen:  "./img/Spectre.jpg"
    },
    {
        tipo: "Pelicula",
        genero: "Drama",
        nombre: "En busca de la felicidad",
        año: 2007,
        popularidad: 4,
        idioma: "Español",
        sinopsis: "Chris Gardner, un joven padre de familia, está tratando de ganarse la vida. Nunca ha tenido un trabajo estable y se pasa los días haciendo malabares. Su mujer en cambio, va en contra de la forma que tiene para salir adelante. Un día, harta de todo, decide abandonar a Chris y a su hijo de cinco años, Christopher. En ese momento, la vida de ambos cambiará para siempre cuando todo se complique y tengan que vivir momentos difíciles, como el embargo de su casa o el esfuerzo en vano por buscar un buen empleo con el que poder mantenerse. Perdido en el peor calvario de su vida, Chris seguirá velando por Christopher, basándose en el afecto y la confianza de su hijo, que se convertirá en la fuerza que le ayudará a superar los obstáculos",
        imagen: "./img/EnBusca.jpg"
    },
    {
        tipo: "Pelicula",
        genero: "Comedia",
        nombre: "Como Dios",
        año: 2003,
        popularidad: 3,
        idioma: "Español",
        sinopsis: "Bruce Nolan, reportero de una famosa cadena de televisión de Búfalo, está siempre de mal humor. Sin embargo, no tiene ninguna razón para esta actitud gruñona: es muy respetado en su trabajo y tiene como pareja a una joven muy guapa, Grace, que le quiere y comparte piso con él. Sin embargo, Bruce es incapaz de verle el lado positivo a las cosas. Luego de un día particularmente malo, Bruce se deja llevar por la rabia y la impotencia y grita y desafía a Dios. Entonces la oreja divina le oye y decide tomar forma humana y bajar a la Tierra para hablar con él y discutir sobre su actitud. Bruce se muestra desafiante ante él, acusándole de tener un trabajo muy fácil, y Dios le propone un peculiar trato al reportero: él le prestar la totalidad de sus poderes divinos durante una semana y entonces ambos verán si Bruce es capaz de hacerlo mejor que él, ya que es tan fácil.",
        imagen: "./img/Bruce.jpg"
    },
    {
        tipo: "Pelicula",
        genero: "Romance",
        nombre: "Yo antes de ti",
        año: 2016,
        popularidad: 3,
        idioma: "Español",
        sinopsis: "Lou Clark (Emilia Clarke) es una entusiasta, ingenua y alegre veinteañera que jamás ha salido de su pueblo, y que debe buscar urgentemente un trabajo para mantener a su familia. En su camino se cruza Will Traynor (Sam Claflin), un exitoso hombre de negocios que también creció en este mismo pueblo, al que ha vuelto tras un accidente accidente de coche que lo dejó impedido en una silla de ruedas. Debido a su condición, este ex aventurero ha caído en una profunda amargura, por lo que cada vez más está decidido a suicidarse. El trabajo de Lou será cuidar a Will. Será entonces cuando la determinación, la dulzura y el optimismo de Lou harán que Will comprenda que la vida es algo que merece la pena vivir. ",
        imagen: "./img/YoAntesDeTi.jpg"
    },
    {
        tipo: "Pelicula",
        genero: "Terror",
        nombre: "El Conjuro",
        año: 2013,
        popularidad: 3,
        idioma: "Español",
        sinopsis: "The Conjuring' cuenta la terrorífica historia de la familia Perron; una familia que, en la década de los 70, afirmó haber contactado con espíritus malvados en su domicilio del estado de Rhode Island. Sus presuntos encuentros sobrenaturales fueron estudiados e investigados por Ed y Lorraine Warren (Interpretados en la película por Patrick Wilson y Vera Farmiga). Lorraine es medium y Ed es un reputado investigador. Ambos se dedican al estudio de casos espirituales y paranormales. Lorraine no tarda en descubrir que las apariciones están relacionadas con el espíritu de un niño y una misteriosa caja de música que lleva en la casa varias décadas.",
        imagen: "./img/Conjuro.jpg"
    },
    {
        tipo: "Pelicula",
        genero: "Terror",
        nombre: "Shutter",
        año: 2006,
        popularidad: 3,
        idioma: "Español",
        sinopsis: "En plena noche, Tun y Jane atropellan accidentalmente a un misterioso peatón, dándose a la fuga. A partir de entonces Jane se ve asediada por terribles pesadillas y Tun, fotógrafo de profesión, percibe extrañas figuras espectrales en sus fotos. Paralelamente, los amigos de Tun empiezan a morir uno a uno. Tanto él como Jane saben muy bien que deben resolver este misterio antes de que sean ellos los próximos en morir.",
        imagen: "./img/Shutter.jpg"
    },
    {
        tipo: "Pelicula",
        genero: "Sci-Fi",
        nombre: "Transformers: La vnganza de los caidos",
        año: 2009,
        popularidad: 2,
        idioma: "Ingles",
        sinopsis: "Han transcurrido dos años desde que Sam Witwicky salvara al universo de una batalla decisiva entre dos clanes rivales de robots extraterrestres. A pesar de sus proezas, Sam sigue siendo un adolescente preocupado por los problemas de la gente de su edad, robots gigantes aparte: pronto entrará en la universidad y debe separarse por primera vez en su vida de su novia Mikaela y de sus padres. También tiene que tratar de explicarle su partida a su nuevo amigo, el robot Bumblebee. Sam aspira a vivir una vida de estudiante normal, pero debe darle la espalda a su destino para lograrlo, y resignarse a vivir una vida todo lo normal que se pueda permitir.",
        imagen: "./img/Transformers.jpg"
    },
    {
        tipo: "Serie",
        genero: "Accion",
        nombre: "Shooter",
        año: 2016,
        popularidad: 4,
        idioma: "Ingles",
        sinopsis: "Shooter es una serie de televisión de género dramático basada en la novela best-seller Point of Impact, escrita por el autor Stephen Hunter y publicada en el año 1993. Este thriller de suspense narra la historia de Bob Lee Swagger (Ryan Phillippe, Secretos y mentiras), un experto francotirador que, desencantado con la guerra, ahora vive en el exilio, dedicando su día a día a las armas de fuego. Pronto, miembros de una organización gubernamental secreta lo intentan convencer para volver a la acción. Sin embargo, la desconfianza de Swagger servirá para que Isaac Johnson (Omar Epps, House), un ex oficial al mando de Swagger ahora convertido en agente secreto, le revele que lo necesita para una operación clandestina por la que deben frustrar un atentado contra el presidente de los Estados Unidos. No obstante, la experiencia de Swagger no será suficiente, y la misión se saldará con resultados desastrosos.",
        imagen: "./img/Shooter.jpg"
    },
    {
        tipo: "Serie",
        genero: "Accion",
        nombre: "Narcos",
        año: 2015,
        popularidad: 5,
        idioma: "Español",
        sinopsis: "'Narcos' es una serie de televisión desarrollada por Netflix que se centra en la historia real de una peligrosa difusión y propagación de una red de cocaína por todo el mundo durante los años 70 y 80. Mientras tanto, las fuerzas de seguridad harán frente a este brutal y sangrante conflicto. La serie está narrada en voz en 'off' desde el presente mientras te cuenta la batalla que hubo entre Escobar y el cartel de Medellín contra el gobierno colombiano respaldado por Washington. Esta ficción dramática sigue a uno de los capos de la cocaína más importante de Colombia, Pablo Escobar. Por otro lado, Javier Peña, agente de la DEA mexicana, es enviado a la localidad para capturar al peligroso líder y aniquilarlo. La serie está protagonizada por Wagner Moura, Pedro Pascal, Boyd Holdbrook, Manolo Cardona y Juan Pablo Raba, entre otros.",
        imagen: "./img/Narcos.jpg"
    },
    {
        tipo: "Serie",
        genero: "Drama",
        nombre: "13 Reasons why",
        año: 2017,
        popularidad: 3,
        idioma: "Ingles",
        sinopsis: "Un día Clay Jensen, uno de estos adolescentes, llega a su casa cuando se encuentra con una misteriosa caja de zapatos sin remitente. Intrigado, decide abrirla y se encuentra con una serie de cintas de cassette, al parecer grabadas por Hannah Baker, una compañera de clase que se suicidó dos semanas antes. Hannah escogió a 13 compañeros para contarles su historia, aquellos 13 a los que culpa de lo que pasó. Gracias a unas instrucciones que dejó en la caja, las cintas deberían llegar a todos ellos para que escucharan una verdad que ninguno quiere oir: las trece razones por las que decidió quitarse la vida. ",
        imagen: "./img/13Reasons.jpg"
    },
    {
        tipo: "Serie",
        genero: "Animacion",
        nombre: "Rick y Morty",
        año: 2014,
        popularidad: 4,
        idioma: "Español",
        sinopsis: "Rick Sánchez es la definición exacta de \"científico loco\". Es alcohólico, es un genio, es irresponsable y está loco. Rick acaba de mudarse a casa de su hija Beth y allí recuerda que tiene un nieto llamado Morty. Sin preguntar a nadie, decide que le va a obligar obligarle a que le acompañe a todo tipo de aventuras para que el chico se vuelva inteligente como él y no se convierta en un idiota como Jerry, padre de Morty y yerno de Rick. Así, Rick y Morty comienzan a vivir aventuras intergalácticas a pesar de que la familia no quiere que lo sigan haciendo. Poco a poco tienen que intentar encontrar un equilibrio entre su vida familiar y sus viajes a través del espacio y por distintas realidades paralelas, algo que no es fácil para el pequeño Morty que es incapaz de tener una vida normal al margen de su abuelo. ",
        imagen: "./img/RickYMorty.jpg"
    },
    {
        tipo: "Serie",
        genero: "Animacion",
        nombre: "Los Simpson",
        año: 1989,
        popularidad: 5,
        idioma: "Español",
        sinopsis: "De la pluma y mente de Matt Groening ('Futurama', 'Los Simpson. La película') en 1989 llegó para quedarse 'Los Simpson', serie de animación que no necesita presentaciones y que es conocida en todo el mundo, aclamada por crítica y público, y que cuenta el día a día de la familia más disparatada de Springfield, sus amigos y el resto de los habitantes de esta ciudad.",
        imagen: "./img/Simpson.jpg"
    },
    {
        tipo: "Serie",
        genero: "Comedia",
        nombre: "Two and a half men",
        año: 2003,
        popularidad: 4,
        idioma: "Ingles",
        sinopsis: "La historia arranca cuando Alan se divorcia y se muda, trayendo a su niño consigo, a la casa de su locuelo hermano. A partir de este momento, la vida de Charlie cambia drásticamente y sólo el cariño y amor que ambos sienten por Jake hará que se pongan de acuerdo, suavizando en cierta medida sus formas de ser, para así poder dar buen ejemplo al pequeño. La 'sitcom' llegó a ser definida en un artículo del New York Times en 2011 como \"la comedia de mayor éxito de la pasada década\".",
        imagen: "./img/TwoAndHalf.jpg"
    },
    {
        tipo: "Serie",
        genero: "Comedia",
        nombre: "El principe de Bel-Air",
        año: 1990,
        popularidad: 4,
        idioma: "Español",
        sinopsis: "Will Smith es un chico bastante alocado del barrio de Filadelfia que pasa la mayor parte del tiempo en la calle. El hecho de que esté continuamente metido en un lio tras otro hace que su madre decida mandarle junto a sus adinerados tíos a Los Ángeles, con el fin de evitar que sucumba a las malas influencias callejeras. ",
        imagen: "./img/BelAir.jpg"
    },
    {
        tipo: "Serie",
        genero: "Romance",
        nombre: "The Affair",
        año: 2004,
        popularidad: 3,
        idioma: "Ingles",
        sinopsis: "La serie muestra dos versiones de la misma trama por separado, una desde el punto de vista masculino y otra desde el femenino. Noah (Dominic West), el marido de unas de las parejas, profesor de instituto y padre de cuatro hijos, conocerá a una mujer que considerará su alma gemela. Y Alison (Ruth Wilson) es una mujer casada intentando recomponer su vida tras haber sufrido una tragedia.",
        imagen: "./img/Affair.jpg"
    },
    {
        tipo: "Serie",
        genero: "Romance",
        nombre: "Tu, yo y ella",
        año: 2016,
        popularidad: 3,
        idioma: "Español",
        sinopsis: "Jack y Emma Trakarsky viven en Portland, en Oregón, y son un matrimonio completamente normal. Ambos estas muy enamorados el uno del otro y quieren pasar el resto de su vida juntos. Pero, desde hace un tiempo, hay algo que falla entre ellos. Han caído en la rutina y les \"chispa\". Todo parece perdido hasta que la solución se presenta cuando conocen a Izzy, una joven independiente y con una mentalidad abierta. Casi sin darse cuenta, este matrimonio empieza una relación poliamorosa con ella que cambiará sus vidas para siempre.",
        imagen: "./img/YouMeHer.jpg"
    },
    {
        tipo: "Serie",
        genero: "Terror",
        nombre: "American Horror Story",
        año: 2011,
        popularidad: 4,
        idioma: "Ingles",
        sinopsis: "La primera de la historias que trata la serie habla sobre la vida de la familia Harmon con tintes terroríficos que recuerdan a 'Paranormal Activity'. Ben (Dylan McDermott 'El Abogado') es un sensible terapeuta que lleva una vida normal hasta que un oscuro secreto le obliga a mudarse a Los Ángeles junto a su familia. Le acompañan su mujer Vivien (Connie Britton 'Friday Night Lights') y la hija que tienen ambos en común, Violet (Taissa Farmiga -hermana pequeña de Vera Farmiga-). La casa que eligen los Harmon para alojarse en Los Ángeles es antigua, tétrica y cuenta con un sótano en el que no sólo habita el polvo, sino también una malvada criatura. Entre el resto de los personajes se encuentran una ama de llaves (Frances Conroy 'A dos metros bajo tierra'), una vecina fisgona llamada Constance (Jessica Lange 'Big Fish'), un hombre quemado que responde al nombre de Larry (Denis O'Hare 'True Blood') y un paciente peligroso de Ben, Tate (Evan Peters 'Kick-Ass. Listo para machacar'), que se fija en la hija de los Harmon.",
        imagen: "./img/HorrorStory.jpg"
    },
    {
        tipo: "Serie",
        genero: "Sci-Fi",
        nombre: "Flash",
        año: 2014,
        popularidad: 4,
        idioma: "Ingles",
        sinopsis: "Barry Allen perdió a su madre con tan sólo 11 años cuando fue asesinada y su padre cargo con el crimen, siendo condenado injustamente. Años más tarde, Barry se convierte en un excelente investigador gracias a su mente privilegiada, algo que aprovecha para intentar averiguar lo que realmente ocurrió en el asesinato de su madre. Sin embargo, todo cambiará cuando sea alcanzado por un rayo mientras trabajaba en un proyecto de acelerador de partículas. Después de estar nueve meses en coma, Barry Allen se despierta y ya no es el mismo. El rayo le ha dotado de una velocidad sobrenatural.",
        imagen: "./img/Flash.jpg"
    },
    {
        tipo: "Serie",
        genero: "Sci-Fi",
        nombre: "Sense8",
        año: 2015,
        popularidad: 5,
        idioma: "Ingles",
        sinopsis: "'Sense8' gira en torno a las aventuras de ocho personas distintas de lugares diferentes del mundo. A raíz de una muerte trágica que perciben a través de los sueños o con visiones, se encuentran en un crecimiento mental y conectado. El grupo variopinto está formado por un conductor de autobús africano, una bloguera americana transexual, un alemán que roba cajas fuertes, una mujer de negocios coreana, un actor de telenovelas mexicanas y una jóven fiestera islandesa. Tras la premonición, intentarán ser reunidos por Jonas, otro 'sensate', mientras que Mr. Whispers irá tras ellos con el mismo poder sensorial.",
        imagen: "./img/Sense8.jpg"
    }
];